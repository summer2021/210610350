import itertools
from re import T
import numpy as np
from mindquantum.gate import X, RZ, RY, CNOT
from mindquantum.circuit import Circuit, AP, A, CPN
from mindquantum.ansatz import Ansatz
from mindquantum.parameterresolver import ParameterResolver as PR

def _single_qubit_excitation_circuit(i, k, theta):
    """
    Implement circuit for single qubit excitation.
    k: creation
    """
    circuit_singles = Circuit()
    circuit_singles += CNOT(i, k)
    circuit_singles += RY(theta).on(k, i)
    circuit_singles += CNOT(i, k)
    return circuit_singles

def _double_qubit_excitation_circuit(i, j, k, l, theta):
    """
    Implement circuit for double qubit excitation.
    k, l: creation
    """
    circuit_doubles = Circuit()
    circuit_doubles += CNOT.on(k, l)
    circuit_doubles += CNOT.on(i, j)
    circuit_doubles += CNOT.on(j, l)
    circuit_doubles += X.on(k)
    circuit_doubles += X.on(i)
    circuit_doubles += RY(theta).on(l, ctrl_qubits=[i, j, k])
    circuit_doubles += X.on(k)
    circuit_doubles += X.on(i)
    circuit_doubles += CNOT.on(j, l)
    circuit_doubles += CNOT.on(i, j)
    circuit_doubles += CNOT.on(k, l)
    return circuit_doubles

def single_qcc_excitation_circuit(n_qubits, n_electrons, generalized=False, spin_sym=True):
    occ_indices = []
    vir_indices = []
    n_orb = n_qubits // 2
    n_orb_occ = int(np.ceil(n_electrons / 2))
    n_orb_vir = n_orb - n_orb_occ
    occ_indices = [i for i in range(n_orb_occ)]
    vir_indices = [i + n_orb_occ for i in range(n_orb_vir)]
    indices_tot = occ_indices + vir_indices

    if generalized:
        iter_set = itertools.combinations(indices_tot, 2)
    else:
        iter_set = itertools.product(occ_indices, vir_indices)
    
    circ = Circuit()
    i = -1
    for (p, q) in iter_set:
        virtual_this = 2 * q
        occupied_this = 2 * p
        # print('(%d\t%d)'%(occupied_this, virtual_this))
        i += 1
        coeff_s = PR({f's{i}': 1})
        circ += _single_qubit_excitation_circuit(virtual_this, occupied_this, coeff_s)
        virtual_this = 2 * q + 1
        occupied_this = 2 * p + 1
        # print('(%d\t%d)'%(occupied_this, virtual_this))
        if spin_sym == True:
            coeff_s = PR({f's{i}': -1})
        else:
            i += 1
            coeff_s = PR({f's{i}': 1})
        circ += _single_qubit_excitation_circuit(virtual_this, occupied_this, coeff_s)
     
    # print(circ)
    return circ

def double_qcc_excitation_circuit(n_qubits, n_electrons, paired=False, generalized=False, spin_sym=True):
    occ_indices = []
    vir_indices = []
    n_orb = n_qubits // 2
    n_orb_occ = int(np.ceil(n_electrons / 2))
    n_orb_vir = n_orb - n_orb_occ
    occ_indices = [i for i in range(n_orb_occ)]
    vir_indices = [i + n_orb_occ for i in range(n_orb_vir)]
    indices_tot = occ_indices + vir_indices
    
    if generalized:
        iter_set = itertools.combinations(indices_tot, 2)
    else:
        iter_set = itertools.product(occ_indices, vir_indices)
    
    circ = Circuit()
    for i, (p, q) in enumerate(iter_set):
        virtual_this = 2 * p
        occupied_this = 2 * q
        virtual_other = 2 * p + 1
        occupied_other = 2 * q + 1
        # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
        coeff_d = PR({f'dp{i}': 1})
        circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
    # print(circ)
    
    if paired:
        return circ
    
    j = -1
    for ((p, q), (r, s)) in itertools.combinations(itertools.product(vir_indices, occ_indices), 2):
        if q > s :
            continue
        if p == r :    # share virtual spatial orbitals    q < s  ;  p = r
            virtual_this = p * 2
            virtual_other = r * 2 + 1
            occupied_this = q * 2
            occupied_other = s * 2 + 1
            j += 1
            coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # circ += A(AP(circuit_doubles, f'dnp{j}'), list((occupied_this, occupied_other, virtual_this, virtual_other)))
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
            occupied_this = q * 2 + 1
            occupied_other = s * 2
            if spin_sym == True:
                coeff_d = PR({f'dnp{j}': -1})
            else:
                j += 1
                coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
        elif q == s :    # share occupied spatial orbitals    q = s  ;  p < r
            occupied_this = q * 2
            occupied_other = s * 2 + 1
            virtual_this = p * 2
            virtual_other = r * 2 + 1
            j += 1
            coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
            virtual_this = p * 2 + 1
            virtual_other = r * 2
            if spin_sym == True:
                coeff_d = PR({f'dnp{j}': -1})
            else:
                j += 1
                coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
        else:         # q < s  ;  p < r
            occupied_this = q * 2
            occupied_other = s * 2
            virtual_this = p * 2
            virtual_other = r * 2
            j += 1
            coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
            occupied_this = q * 2 + 1
            occupied_other = s * 2 + 1
            virtual_this = p * 2 + 1
            virtual_other = r * 2 + 1
            if spin_sym == True:
                coeff_d = PR({f'dnp{j}': 1})
            else:
                j += 1
                coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))

            occupied_this = q * 2
            occupied_other = s * 2 + 1
            virtual_this = p * 2
            virtual_other = r * 2 + 1
            j += 1
            coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
            occupied_this = q * 2 + 1
            occupied_other = s * 2
            virtual_this = p * 2 + 1
            virtual_other = r * 2
            if spin_sym == True:
                coeff_d = PR({f'dnp{j}': 1})
            else:
                j += 1
                coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))

            occupied_this = q * 2
            occupied_other = s * 2 + 1
            virtual_this = p * 2 + 1
            virtual_other = r * 2
            j += 1
            coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
            occupied_this = q * 2 + 1
            occupied_other = s * 2
            virtual_this = p * 2
            virtual_other = r * 2 + 1
            if spin_sym == True:
                coeff_d = PR({f'dnp{j}': 1})
            else:
                j += 1
                coeff_d = PR({f'dnp{j}': 1})
            circ += _double_qubit_excitation_circuit(virtual_other, virtual_this, occupied_other, occupied_this, coeff_d)
            # print('(%d\t%d\t%d\t%d)'%(occupied_this, occupied_other, virtual_this, virtual_other))
        
    # print(circ)

    return circ

def qccsd_singlet_generator(n_qubits, n_electrons, spin_sym=False):
    circuit_1 = single_qcc_excitation_circuit(n_qubits, n_electrons, spin_sym=spin_sym)
    circuit_2 = double_qcc_excitation_circuit(n_qubits, n_electrons, spin_sym=spin_sym)
    circuit = circuit_1 + circuit_2
    return circuit

def k_qpccgsd_singlet_generator(n_qubits, n_electrons, depth, spin_sym=True):
    circuit = Circuit()
    circuit_s_g = single_qcc_excitation_circuit(n_qubits, n_electrons, generalized=True, spin_sym=spin_sym)
    circuit_d_g = double_qcc_excitation_circuit(n_qubits, n_electrons, paired=True, generalized=True, spin_sym=spin_sym)
    
    for i_depth in range(depth):
        circuit += AP(circuit_s_g, f'k{i_depth}')
        circuit += AP(circuit_d_g, f'k{i_depth}')
    
    return circuit

def add_prefix_to_circuit(circuit, str_prefix):
    res_circuit = AP(circuit, str_prefix)
    return res_circuit

if __name__ == "__main__":
    circuit = k_qpccgsd_singlet_generator(12, 4, 3)
    # print(circuit)
    print(circuit.summary())
    print(circuit.parameter_resolver())