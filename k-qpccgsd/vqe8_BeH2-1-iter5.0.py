import os
import sys
import numpy as np
from openfermion.chem import MolecularData
from openfermion.transforms import get_fermion_operator, jordan_wigner

from mindquantum import Circuit, X, RX, Hamiltonian
from mindquantum.nn import generate_pqc_operator
from mindspore import Tensor
import mindspore.context as context
from qcc_excitation_circuit import k_qpccgsd_singlet_generator
from scipy.optimize import minimize

# np.set_printoptions(threshold=np.inf)
os.environ['OMP_NUM_THREADS'] = '8'
context.set_context(mode=context.GRAPH_MODE, device_target="CPU")
TOLERANCE = 1e-6

def print_state(state_array, n_qubits):
    for i, value in enumerate(state_array):
        if abs(value) > TOLERANCE:
            ang = np.angle(state_array[i])/np.pi
            ket = bin(i)[2:].zfill(n_qubits)
            spin_down_num = ket[::2].count('1')
            spin_up_num = ket[1::2].count('1')
            print("%7.4f %7.4f%s |%s>  %d  %d" % (float(abs(value)), float(ang), chr(960), ket, spin_down_num, spin_up_num))

def call_fun_IterStep(paras, mol_pqc, res_last={}):
    if 'iter_no' not in res_last:
        res_last['iter_no'] = 0
    print("Step %3d energy    %20.16f    %20.16f    %20.16f" %(res_last['iter_no'], res_last['f'], res_last['f']-res_last['f_last'], res_last['grad_norm']))
    res_last['iter_no'] += 1
    # print("Step %3d energy    %20.16f    %20.16f    %20.16f" % (iter_no, float(energy), float(energy_diff), float(ansatz_grad_norm)))

def energy_obj(n_paras, mol_pqc, res_last={}):
    encoder_data = Tensor(np.array([[0]]).astype(np.float32))
    ansatz_data = Tensor(np.array(n_paras).astype(np.float32))
    e, _, grad = mol_pqc(encoder_data, ansatz_data)

    if 'f_last' not in res_last:
        res_last['f_last'] = 0.0
    else:
        res_last['f_last'] = res_last['f']
    
    res_last['x'] = list(n_paras)
    res_last['grad'] = list(grad.asnumpy()[0, 0])
    res_last['grad_norm'] = np.sqrt(np.sum(grad.asnumpy()[0, 0]**2))
    res_last['f'] = e.asnumpy()[0, 0]
    return e.asnumpy()[0, 0], grad.asnumpy()[0, 0]

def vqe(dist, depth, init_amplitudes=[]):
    geometry = [
        ["Be", [0.0, 0.0, 0.0]],
        ["H", [dist, 0.0, 0.0]],
        ["H", [-1.0 * dist, 0.0, 0.0]],
    ]
    basis = "sto3g"
    spin = 0
    multiplicity = 2 * spin + 1
    description = '{:.3f}'.format(dist)
    molecule = MolecularData(geometry, basis, multiplicity, description=description)
    molecule.load()
    hamiltonian_InteractionOperator = molecule.get_molecular_hamiltonian()
    hamiltonian_FermionOperator = get_fermion_operator(hamiltonian_InteractionOperator)
    hamiltonian_QubitOperator = jordan_wigner(hamiltonian_FermionOperator)
    hamiltonian_QubitOperator.compress()
    for term in hamiltonian_QubitOperator.terms:
        hamiltonian_QubitOperator.terms[term] = hamiltonian_QubitOperator.terms[term].real

    hf_state_circuit = Circuit([X.on(i) for i in range(0,molecule.n_electrons,1)])
    qccsd_circuit = k_qpccgsd_singlet_generator(molecule.n_qubits, molecule.n_electrons, depth)
    total_circuit = hf_state_circuit + qccsd_circuit
    if len(init_amplitudes) == 0:
        init_amplitudes = [0.0 for j in range(len(total_circuit.para_name))]

    molecule_pqc = generate_pqc_operator(["null"], total_circuit.para_name, RX("null").on(0) + total_circuit, Hamiltonian(hamiltonian_QubitOperator))

    # print(total_circuit.summary())
    # print(total_circuit.parameter_resolver())
    print(init_amplitudes)
    
    res_last={}
    res = minimize(energy_obj,
        init_amplitudes,
        args=(molecule_pqc, res_last),
        method='bfgs',
        options={'disp': True, 'return_all': True},
        jac=True,
        callback=call_fun_IterStep, 
        tol=1e-6)
        
    print("VQE energy with QCC ansatz:{}".format(float(res.fun)))
    print("Corresponding parameters:{}".format(res.x.tolist()))

    # if print_tag == True:
    #     # 量子态展示
    #     net = MAL(total_circuit.para_name, total_circuit, Hamiltonian(hamiltonian_QubitOperator))
    #     pr = dict(zip(total_circuit.para_name, net.weight.asnumpy()))
    #     state_array = StateEvolution(total_circuit).final_state(pr)
    #     print_state(state_array, n_qubits)

    if molecule.ccsd_energy == None:
        molecule.ccsd_energy = 0.0
    return molecule.hf_energy, molecule.ccsd_energy, molecule.fci_energy, float(res.fun), res.x.tolist()

depth = 1
dist0 = 5.0
file_name1 ='/BeH2/BeH2-res-{}-iter{}.txt'.format(depth, dist0)
file_name2 ='/BeH2/BeH2-amplitudes-{}-iter{}.txt'.format(depth, dist0)
print(file_name1)
print(file_name2)

with open(sys.path[0] + file_name1, 'a+', buffering=1) as f1, open(sys.path[0] + file_name2, 'a+', buffering=1) as f2:
    for idx, dist in enumerate(np.arange(dist0, 0.85, -0.05)):
        print('%6.4f\t%4d'%(dist, depth))
        if idx == 0:
            energy_hf, energy_ccsd, energy_fci, energy_vqe, final_amplitudes = vqe(dist, depth)
        else:
            energy_hf, energy_ccsd, energy_fci, energy_vqe, final_amplitudes = vqe(dist, depth, final_amplitudes)
        f1.write("%6.4f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
        f2.write("%6.4f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
        f2.write(str(final_amplitudes))
        f2.write("\n")
        print("%6.4f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))


