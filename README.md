hardware_efficient_circuit.py: 用以替换mindquantum.ansatz中的HardwareEfficientAnsatz接口，新接口不仅保留了原有接口的一切功能，同时还支持基于现有的模块化的线路构造新的ansatz

HE_CNOT_linear_vqe.py: 构建如下图所示的线路，并计算LiH分子的能量,其中线路深度可由代码中的depth变量来调控
![Image text](https://gitlab.summer-ospp.ac.cn/summer2021/210610350/-/blob/main/pictures/pic-1.jpg)

HE_CNOT_alternate_vqe.py: 构建如下图所示的线路，并计算LiH分子的能量,其中线路深度可由代码中的depth变量来调控
![Image text](https://gitlab.summer-ospp.ac.cn/summer2021/210610350/-/blob/main/pictures/pic-2.jpg)

HE_ExchangeGate_alternate_vqe.py: 构建如下图所示的线路，并计算LiH分子的能量,其中线路深度可由代码中的depth变量来调控
![Image text](https://gitlab.summer-ospp.ac.cn/summer2021/210610350/-/blob/main/pictures/pic-3.jpg)

HE_ExchangeGate_linear_vqe.py: 构建如下图所示的线路，并计算LiH分子的能量,其中线路深度可由代码中的depth变量来调控
![Image text](https://gitlab.summer-ospp.ac.cn/summer2021/210610350/-/blob/main/pictures/pic-4.jpg)

qccsd_uccsd文件夹：qccsd与uccsd的样例程序

k-qpccgsd文件夹：k-QpCCGSD的样例程序

k-upccgsd文件夹：k-UpCCGSD的样例程序

注：部分程序在运行时，运行到
minimize(energy_obj,
        init_amplitudes,
        args=(molecule_pqc, res_last),
        method='bfgs',
        options={'disp': True, 'return_all': True},
        jac=True,
        callback=call_fun_IterStep, 
        tol=1e-6)
时可能会报错，有两种解决方法，一是去掉callback=call_fun_IterStep这句话，但这样做的话就不能看到优化过程的输出结果；另一个方法是修改scipy的源代码，修改scipy/optimize/optimize.py文件中的_minimize_bfgs函数，将
if callback is not None:
    callback(xk)
改为
if callback is not None:
    callback(xk, *args)

