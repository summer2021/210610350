import os
import sys
os.environ['OMP_NUM_THREADS'] = '4'
import numpy as np
from mindspore import Tensor
import mindspore as ms
from mindquantum import Circuit, X, RX, Hamiltonian
from mindquantum.nn import generate_pqc_operator
from scipy.optimize import minimize
from openfermionpyscf import run_pyscf
from openfermion.chem import MolecularData
from multiprocessing import Pool as ThreadPool
from ucc import generate_upccgsd

ms.context.set_context(mode=ms.context.GRAPH_MODE, device_target="CPU")

def energy_obj(n_paras, mol_pqc):
    encoder_data = Tensor(np.array([[0]]).astype(np.float32))
    ansatz_data = Tensor(np.array(n_paras).astype(np.float32))
    e, _, grad = mol_pqc(encoder_data, ansatz_data)
    return e.asnumpy()[0, 0], grad.asnumpy()[0, 0]

def HE_vqe_test(geometry, depth):
    basis = 'sto3g'
    multiplicity = 1
    molecule_of = MolecularData(geometry, basis, multiplicity)
    molecule_of = run_pyscf(molecule_of, run_scf=1, run_ccsd=1, run_fci=1)
    hartreefock_wfn_circuit = Circuit([X.on(i) for i in range(molecule_of.n_electrons)])
    ansatz_circuit, init_amplitudes, ansatz_parameter_names, hamiltonian_QubitOp, n_qubits, n_electrons = generate_upccgsd(molecule_of, th = -1, k = depth)

    hamiltonian_QubitOp = hamiltonian_QubitOp.real
    total_circuit = hartreefock_wfn_circuit + ansatz_circuit

    total_pqc = generate_pqc_operator(["null"], ansatz_parameter_names, \
                                RX("null").on(n_qubits-1) + total_circuit, \
                                Hamiltonian(hamiltonian_QubitOp))

    #para_only_energy_obj = partial(energy_obj, hea_circuit, q_ham)
    
    n_paras = len(total_circuit.para_name)
    paras = init_amplitudes

    res = minimize(energy_obj,
            paras,
            args=(total_pqc, ),
            method='bfgs',
            options={'disp': True, 'return_all': True},
            jac=True,
            tol=1e-6)
    
    return molecule_of.hf_energy, molecule_of.ccsd_energy, molecule_of.fci_energy, float(res.fun)


with open(sys.path[0] + '/H6.txt', 'a+', buffering=1) as f:
    for idx, dist in enumerate(np.arange(0.5, 2.5, 0.05)):
        geometry = [
            ["H", [0.0, 0.0, 0.0]],
            ["H", [dist, 0.0, 0.0]],
            ["H", [dist * 2.0, 0.0, 0.0]],
            ["H", [dist * 3.0, 0.0, 0.0]],
            ["H", [dist * 4.0, 0.0, 0.0]],
            ["H", [dist * 5.0, 0.0, 0.0]],
        ]
        energy_vqe_list = []
        for depth in range(1, 5):
            print('%6.4f\t%4d'%(dist, depth))
            energy_hf, energy_ccsd, energy_fci, energy_vqe = HE_vqe_test(geometry, depth)
            energy_vqe_list.append(energy_vqe)
        f.write("%6.4f\t%20.16f\t%20.16f\t%20.16f"%(dist, energy_hf, energy_ccsd, energy_fci))
        for energy_i in energy_vqe_list:
            f.write("\t%20.16f"%(energy_i))
        f.write("\n")
        print("%6.4f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci))
        for energy_i in energy_vqe_list:
            print("\t%20.16f"%(energy_i),end='')
        print()


