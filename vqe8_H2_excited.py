import os
import sys
import random
import numpy as np
from openfermion.chem import MolecularData
from openfermion.transforms import get_fermion_operator, jordan_wigner
from mindquantum import Circuit, X, RX, RY, Hamiltonian, StateEvolution
from mindquantum.nn import generate_pqc_operator
from mindquantum.parameterresolver import ParameterResolver
from mindspore import Tensor
import mindspore.context as context
from qcc_excitation_circuit import k_qpccgsd_singlet_generator, add_prefix_to_circuit
from scipy.optimize import minimize

# np.set_printoptions(threshold=np.inf)
os.environ['OMP_NUM_THREADS'] = '8'
context.set_context(mode=context.GRAPH_MODE, device_target="CPU")
TOLERANCE = 1e-4
overlap_punish_factor = 10.0

def print_state(state_array, n_qubits):
    for i, value in enumerate(state_array):
        if abs(value) > TOLERANCE:
            ang = np.angle(state_array[i])/np.pi
            ket = bin(i)[2:].zfill(n_qubits)
            spin_down_num = ket[::2].count('1')
            spin_up_num = ket[1::2].count('1')
            print("%7.4f %7.4f%s |%s>  %d  %d" % (float(abs(value)), float(ang), chr(960), ket, spin_down_num, spin_up_num))

def call_fun_IterStep(paras, mol_pqc, ansatz_circuit, ground_state_circuit, ground_state_paras, res_last={}):
    if 'iter_no' not in res_last:
        res_last['iter_no'] = 0
    print("Step %3d energy    %20.16f    %20.16f    %20.16f    %8.6f" \
        %(res_last['iter_no'], res_last['f'], res_last['f']-res_last['f_last'], res_last['grad_norm'], res_last['overlap']))
    res_last['iter_no'] += 1
    # print("Step %3d energy    %20.16f    %20.16f    %20.16f" % (iter_no, float(energy), float(energy_diff), float(ansatz_grad_norm)))

def circuit_grad(qubit_circuit, paras_data, obj_grad_paras):
    obj_paras_grad = []

    state_array = StateEvolution(qubit_circuit).final_state(paras_data)

    for obj_grad_para in obj_grad_paras:
        para_grad = 0.0
        for gate_idx, gate in enumerate(qubit_circuit):
            grad_circuit = Circuit()
            grad_circuit += qubit_circuit
            if gate.coeff is None:
                continue
            if isinstance(gate.coeff, ParameterResolver) == False:
                continue
            if gate.coeff.para_name[0] == obj_grad_para:
                add_gate = RY(np.pi).on(gate.obj_qubits, gate.ctrl_qubits)
                grad_coeff = 0.5 * gate.coeff.para_value[0]
                grad_circuit.insert(gate_idx, add_gate)
                state_array0 = StateEvolution(grad_circuit).final_state(paras_data)
                para_grad += grad_coeff * state_array0[0] * state_array[0] * 2.0
                continue
        # print(para_grad)
        obj_paras_grad.append(para_grad.real)
    # print(obj_paras_grad)
    return np.array(obj_paras_grad)
        

def energy_obj(n_paras, mol_pqc, ansatz_circuit, ground_state_circuit, ground_state_paras, res_last={}):
    encoder_data = Tensor(np.array([[0]]).astype(np.float32))
    ansatz_data = Tensor(np.array(n_paras).astype(np.float32))
    e, _, energy_grad0 = mol_pqc(encoder_data, ansatz_data)
    energy = e.asnumpy()[0, 0]
    energy_grad = energy_grad0.asnumpy()[0, 0]

    overlap_circuit = ansatz_circuit + ground_state_circuit.hermitian
    pr = dict(zip(overlap_circuit.para_name, list(n_paras) + ground_state_paras[::-1]))
    state_array = StateEvolution(overlap_circuit).final_state(pr)
    overlap = float(abs(state_array[0]))**2
    overlap_grad = circuit_grad(overlap_circuit, pr, ansatz_circuit.para_name)
    
    # # test overlap_grad
    # print(overlap_grad)
    # eps = 1e-6
    # for i in range(8000):
    #     pr['k0_s0'] = pr['k0_s0'] + eps
    #     state_array1 = StateEvolution(overlap_circuit).final_state(pr)
    #     overlap1 = float(abs(state_array1[0]))**2
    #     print(eps*(i+1), (overlap1-overlap)/(eps*(i+1)))

    f = energy + overlap_punish_factor * overlap
    grad = energy_grad + overlap_punish_factor * overlap_grad

    if 'f_last' not in res_last:
        res_last['f_last'] = 0.0
    else:
        res_last['f_last'] = res_last['f']
    
    res_last['x'] = list(n_paras)
    res_last['overlap'] = overlap
    res_last['grad'] = list(grad)
    res_last['grad_norm'] = np.sqrt(np.sum(grad**2))
    res_last['f'] = f
    res_last['f_true'] = energy
    return f, grad


def vqe(dist, depth, init_amplitudes=[]):
    geometry = [
        ["H", [0.0, 0.0, 0.0]],
        ["H", [dist, 0.0, 0.0]],
    ]
    basis = "sto3g"
    spin = 0
    multiplicity = 2 * spin + 1
    description = '{:.3f}'.format(dist)
    molecule = MolecularData(geometry, basis, multiplicity, description=description)
    molecule.load()
    hamiltonian_InteractionOperator = molecule.get_molecular_hamiltonian()
    hamiltonian_FermionOperator = get_fermion_operator(hamiltonian_InteractionOperator)
    hamiltonian_QubitOperator = jordan_wigner(hamiltonian_FermionOperator)
    hamiltonian_QubitOperator.compress()

    hf_state_circuit = Circuit([X.on(i) for i in range(0,molecule.n_electrons,1)])
    qccsd_circuit = k_qpccgsd_singlet_generator(molecule.n_qubits, molecule.n_electrons, depth)
    total_circuit = hf_state_circuit + qccsd_circuit
    ground_state_circuit = add_prefix_to_circuit(total_circuit, 'g0')
    total_circuit = hf_state_circuit + k_qpccgsd_singlet_generator(molecule.n_qubits, molecule.n_electrons, 2, spin_sym=False)
    init_amplitudes = [random.random() for j in range(len(total_circuit.para_name))]
    print(init_amplitudes)
    ground_state_amplitudes = [-2.5437139710730514e-05, -0.3527054024157375]
    
    # if len(init_amplitudes) == 0:
    #     init_amplitudes = [0.7905293216763384, 0.4007799108541353, 1.3211182819226794, 0.03880195112848756, -1.94292772092074, 0.7422279253441766, 3.256649238981062, 0.9001577013506068, 4.764453000652176, 2.228500465735774, 0.060789388713599804, 2.94450980270469, -0.043479932413782944, 2.1539919806835575, 0.004292370754841327, -1.4248527952505985, -0.0669873312453633, -0.11063431752067968, -0.6772319870497957, -0.10055854455305906, -0.08085240217400627, -0.06485177044289746, 1.5354980922545372, 0.04200217204287206, 0.41727505006455945, 0.21694234241104096, -0.7670083586058907, -0.034223806500361775, -1.4993227044948643, 0.019194232418285946, -0.00974366503948479, -0.059653548266179184, -0.22507326567918123, 1.8375354604868839, 0.052359190722069154, 0.012403757522647585, 0.05072846502558559, 0.34658634886347894, 0.07479125107944339, 0.35844917797352055, 2.0941490303548904, 0.15565144151635601, 2.4267612982014635, 1.3422858994498035, 1.0766617881281872, 2.3148153388317514, -1.3983272285815986, -0.10857655752712063, 0.0484024213440354, 0.030018900085173588, -0.7379210948076261, -0.00813819765604075, 0.05016659844684415, 0.022921032898183907, -0.007133145170406673, -0.03453120753505692, 0.024330795332020767, 2.7382588413524664, 1.503114017828997, 3.403786779522329, 1.4246185670657359, 0.8743839973444978, 0.14972013243384488, 3.134554752462698, 1.7974687766983348, -0.5737035483162124, -0.0458062277832441, 1.1276592822284401, -0.08845312134290514, 0.010494326963492725, -0.5929141762285511, -0.11002000797764702, 0.08966407834257928, 0.55136782785937, -0.11226935890167417, -0.09122067419866735, -0.04117319555325047, -0.3859343753814169, -0.0638640211612721, -1.7244722719993244, -1.7211864862207547, 0.05353099867151329, 0.25671768426594377, 0.07126410073153495, -0.006643339279574033, -2.215896122100686, 0.20844002311979792, -0.04334487842126735, -5.051029990370338, -0.14912755736430147, 0.032756545535601436, -0.06471352765174894, 0.6272151507392799, -0.04197457522636202, 0.2842341281817413, 1.5315634674730911, 3.1638365903452277, -0.019681428222425988, 1.2876353617262206, 2.9650412597594533, 0.17568834754111495, 3.140948811621573, -0.2250903266180829, -1.442912424802624, 2.559394105688655, -0.27981528484855106, -5.743727410901584, -0.05144095605310563, -0.09683713620366799, 0.07932153542890456, -1.174811431739525, -0.17246553035537557, -0.18496188559942986, -0.0937813798937732, -0.39192716275203654, -0.043230417587660025, -0.06091959399655368, 0.1045525239443452, -0.6315604710360021, 1.9095609502001054]

    # print("init_amplitudes:\n{}".format(init_amplitudes))
    molecule_pqc = generate_pqc_operator(["null"], total_circuit.para_name, RX("null").on(0) + total_circuit, Hamiltonian(hamiltonian_QubitOperator))
    
    # print(total_circuit.summary())
    # print(total_circuit.parameter_resolver())
    # energy_obj(init_amplitudes, molecule_pqc, total_circuit, ground_state_circuit, ground_state_amplitudes)

    res_last={}
    res = minimize(energy_obj,
        init_amplitudes,
        args=(molecule_pqc, total_circuit, ground_state_circuit, ground_state_amplitudes, res_last),
        method='bfgs',
        options={'disp': True, 'return_all': True},
        jac=True,
        callback=call_fun_IterStep, 
        tol=1e-6)
        
    print("VQE energy with QCC ansatz:{}".format(float(res.fun)))
    print("Corresponding parameters:\n{}".format(res.x.tolist()))
    print(res_last['overlap'])
    print(res_last['f_true'])

    # 量子态展示
    pr = dict(zip(total_circuit.para_name, res.x.tolist()))
    state_array = StateEvolution(total_circuit).final_state(pr)
    print_state(state_array, molecule.n_qubits)
 
    return molecule.hf_energy, molecule.ccsd_energy, molecule.fci_energy, float(res.fun), res.x.tolist()

for i in range(100):
    depth = 1
    dist = 1.0
    print('%6.4f\t%4d'%(dist, depth))
    energy_hf, energy_ccsd, energy_fci, energy_vqe, final_amplitudes = vqe(dist, depth)
    print("%6.4f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
    break
    
'''
VQE energy with QCC ansatz:-0.7458717823027068
Corresponding parameters:
[1.8854018090164177, 1.8853792419726476, -3.741770149575544, 0.42707246371008395, 1.2396115714621705, 3.503774987801415]
7.493162558511577e-15
-0.7458718
 0.7071  1.0000π |0110>  1  1
 0.7071  1.0000π |1001>  1  1
'''

# with open(sys.path[0] + '/H6/H6-res-1.txt', 'a+', buffering=1) as f1, open(sys.path[0] + '/H6/H6-amplitudes-1.txt', 'a+', buffering=1) as f2:
#     for idx, dist in enumerate(np.arange(0.95, 0.45, -0.05)):
#         depth = 4
#         print('%6.4f\t%4d'%(dist, depth))
#         if idx == 0:
#             energy_hf, energy_ccsd, energy_fci, energy_vqe, final_amplitudes = vqe(dist, depth)
#         else:
#             energy_hf, energy_ccsd, energy_fci, energy_vqe, final_amplitudes = vqe(dist, depth, final_amplitudes)
#         f1.write("%6.4f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
#         f2.write("%6.4f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
#         f2.write(str(final_amplitudes))
#         f2.write("\n")
#         print("%6.4f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
        