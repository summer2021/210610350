import os
from mindquantum.gate.basicgate import RZ
import numpy as np
from matplotlib import pyplot as plt 
from openfermionpyscf import run_pyscf
import mindquantum as mq
from mindquantum import Circuit, X, RX, RY, Hamiltonian, StateEvolution
from mindquantum.circuit import generate_uccsd
from mindquantum.nn import generate_pqc_operator
import mindspore as ms
from mindspore import Tensor
from mindspore import dtype as mstype
import mindspore.context as context
from mindspore.common.parameter import Parameter
from mindspore.common.initializer import initializer
from openfermion.chem import MolecularData
from mindquantum.nn import MindQuantumAnsatzOnlyLayer as MAL
from mindquantum.ops import QubitOperator
from mindquantum.hiqfermion.ucc import get_qubit_hamiltonian
from openfermion.ops import InteractionOperator
from openfermion.transforms import get_fermion_operator, jordan_wigner
from hardware_efficient_circuit import HardwareEfficientAnsatz

# np.set_printoptions(threshold=np.inf)
context.set_context(mode=context.GRAPH_MODE, device_target="CPU")
TOLERANCE = 1e-12

def print_state(state_array, n_qubits):
    for i, value in enumerate(state_array):
        if abs(value) > TOLERANCE:
            ang = np.angle(state_array[i])/np.pi
            ket = bin(i)[2:].zfill(n_qubits)
            # print(abs(value),':',ang, chr(960),'---',value,'|', ket, '>')
            print("%7.4f %7.4f%s |%s>" % (float(abs(value)), float(ang), chr(960), ket))


def HE_vqe_test(print_tag=True):
    dist = 1.5
    geometry = [
        ["Li", [0.0, 0.0, 0.0 * dist]],
        ["H", [0.0, 0.0, 1.0 * dist]],
    ]
    basis = "sto3g"
    spin = 0

    molecule_of = MolecularData(geometry, basis, multiplicity=2 * spin + 1)
    molecule_of = run_pyscf(molecule_of, run_scf=1, run_ccsd=1, run_fci=1)

    molecule_of.save()
    molecule_file = molecule_of.filename

    hf_state_circuit = Circuit(
        [X.on(i) for i in range(0,molecule_of.n_electrons,1)])
    # print(hf_state_circuit)

    hamiltonian_QubitOp = get_qubit_hamiltonian(molecule_of)
    n_qubits = molecule_of.n_qubits
    n_electrons = molecule_of.n_electrons
    hamiltonian_InteractionOperator = molecule_of.get_molecular_hamiltonian()
    hamiltonian_FermionOperator=get_fermion_operator(hamiltonian_InteractionOperator)
    
    # # The number of particles needs to be conserved, corresponding  penalty term is added into the Hamiltonian. 
    # # If the number of particles is not conserved, the total energy will be increased
    # particle_num_punish_coeff =2.0
    # particle_num_cons_InteractionOperator = InteractionOperator(-1.0*float(n_electrons), np.eye(n_qubits), np.zeros((n_qubits, n_qubits, n_qubits, n_qubits)))
    # particle_num_cons_FermionOperator = get_fermion_operator(particle_num_cons_InteractionOperator)
    # particle_num_cons_punish_FermionOperator = particle_num_punish_coeff * particle_num_cons_FermionOperator * particle_num_cons_FermionOperator
    # # The number of spin up electrons should be equal to spin down, corresponding penalty term is added into the Hamiltonian
    # # If spin up electrons can't be cancelled with spin down electrons, the total energy will be increased
    # spin_num_punish_coeff =2.0
    # spin_num_cons_matrix = np.zeros((n_qubits, n_qubits)) 
    # for i in np.arange(n_qubits):
    #     if i % 2 == 0:
    #         spin_num_cons_matrix[i,i] = 1
    #     else:
    #         spin_num_cons_matrix[i,i] = -1
    # spin_num_cons_InteractionOperator = InteractionOperator(0.0, spin_num_cons_matrix, np.zeros((n_qubits, n_qubits, n_qubits, n_qubits)))
    # spin_num_cons_FermionOperator = get_fermion_operator(spin_num_cons_InteractionOperator)
    # spin_num_cons_punish_FermionOperator = spin_num_punish_coeff * spin_num_cons_FermionOperator * spin_num_cons_FermionOperator

    eff_hamiltonian_FermionOperator = hamiltonian_FermionOperator # + particle_num_cons_punish_FermionOperator + spin_num_cons_punish_FermionOperator
    eff_hamiltonian_QubitOperator = jordan_wigner(eff_hamiltonian_FermionOperator)
    eff_hamiltonian_QubitOperator.compress()

    empty_circ = Circuit()
    entangle_circ = Circuit()
    entangle_circ += X.on(1,0)
    entangle_circ += RZ({'phi':-1.0}).on(0)
    entangle_circ += RZ(np.pi/2).on(0)
    entangle_circ += X.on(0,1)
    entangle_circ += RZ(np.pi/2).on(0)
    entangle_circ += RY('theta').on(0)
    entangle_circ += X.on(0,1)
    entangle_circ += RY({'theta':-1.0}).on(0)
    entangle_circ += RZ(-1.0 * np.pi).on(0)
    entangle_circ += RZ(-1.0 * np.pi/2).on(1)
    entangle_circ += X.on(1,0)
    entangle_circ += RZ('phi').on(0)
    # print(entangle_circ)

    n_qubits=molecule_of.n_qubits
    depth = 1
    ansatz0 = HardwareEfficientAnsatz(n_qubits, [], entangle_circ, 'linear', depth)
    # ansatz1 = HardwareEfficientAnsatz(n_qubits, [], entangle_circ, 'alternate', depth)
    total_circuit = hf_state_circuit + ansatz0.circuit # + ansatz1.circuit
    # print(total_circuit)

    molecule_pqc = generate_pqc_operator(["null"], total_circuit.para_name, RX("null").on(0) + total_circuit, Hamiltonian(eff_hamiltonian_QubitOperator))
    net = MAL(total_circuit.para_name, total_circuit, Hamiltonian(eff_hamiltonian_QubitOperator))
    opti = ms.nn.Adagrad(net.trainable_params(), learning_rate=4e-1)
    train_net = ms.nn.TrainOneStepCell(net, opti)
    if print_tag == True:
        print(total_circuit.summary())
        print(total_circuit.parameter_resolver())
        print(net.trainable_params())

    eps = 1.e-8
    iter_idx = 0
    min_iter = 5
    energy_diff = eps * 1000
    energy_last = np.zeros(1).astype(np.float32)
    energy_list = np.zeros([0]).astype(np.float32)
    energy_diff_list = np.zeros([0]).astype(np.float32)
    encoder_data = Tensor(np.zeros([1,1]).astype(np.float32))

    # def energy_objective(packed_amplitudes):
    #     """
    #     The objective function.
    #     """
    #     measure_result, encoder_grad, ansatz_grad = molecule_pqc(encoder_data, net.weight)


    #     wavefunction = compiler_engine.allocate_qureg(n_qubits)
    #     for i in range(n_ele):
    #         X | wavefunction[i]
    #     evolution_operator = uccsd_singlet_evolution(packed_amplitudes, n_qubits, n_ele)
    #     evolution_operator | wavefunction
    #     compiler_engine.flush()
    #     energy = compiler_engine.backend.get_expectation_value(
    #         qubit_hamiltonian, wavefunction)
    #     All(Measure) | wavefunction
    #     compiler_engine.flush(deallocate_qubits=True)
        
    #     return energy

    

    while (abs(energy_diff) > eps) or (iter_idx < min_iter):
        train_net()
        measure_result, encoder_grad, ansatz_grad = molecule_pqc(encoder_data, net.weight)
        energy_i = measure_result.asnumpy()
        ansatz_grad_norm = np.sqrt(np.sum(ansatz_grad.asnumpy()[0,0]**2))
        energy_diff = energy_i - energy_last
        energy_last = energy_i
        energy_list = np.append(energy_list, energy_i)
        energy_diff_list = np.append(energy_diff_list, energy_diff)
        if ((iter_idx % 1 == 0) and (print_tag == True)):
            print("Step %3d energy    %20.16f    %20.16f    %20.16f" % (iter_idx, float(energy_i), float(energy_diff), float(ansatz_grad_norm)))
            # print(net.weight.asnumpy())
            # pr = dict(zip(total_circuit.para_name, net.weight.asnumpy()))
            # print(StateEvolution(total_circuit).final_state(pr, ket=True))
            # print(ansatz_grad.asnumpy()[0,0])
        iter_idx += 1
        
    
    

    if print_tag == True:
        print("Optimization completed at step %3d" % (iter_idx - 1))
        print("Optimized energy: %20.16f" % (energy_i))
        # print("Optimized amplitudes: \n", net.weight.asnumpy())
        # print("net.construct(): \n", net.construct())

        # 量子态展示
        pr = dict(zip(total_circuit.para_name, net.weight.asnumpy()))
        state_array = StateEvolution(total_circuit).final_state(pr)
        print_state(state_array, n_qubits)
        # print(StateEvolution(total_circuit).final_state(pr, ket=True))


        # 量子态的概率分布,量子比特数较多时慎用
        # def show_amp(state, bar_num):
        #     low_lim = -1 * bar_num
        #     amp = np.abs(state)**2
        #     labels = [bin(i)[2:].zfill(n_qubits) for i in range(len(amp))]
            
        #     amp_sort = amp.argsort()[low_lim::]
        #     amp_sort.sort()
            
        #     amp = amp[amp_sort]
        #     labels = np.array(labels)
        #     labels = labels[amp_sort]
        #     plt.bar(labels, amp)
        #     plt.xticks(rotation=30)
        #     plt.show()
        # pr = dict(zip(total_circuit.para_name, net.weight.asnumpy()))
        # state = StateEvolution(total_circuit).final_state(pr)
        # # print(state)
        # bar_num = 5
        # show_amp(state, bar_num)

        # 收敛曲线图
        # x = np.arange(0,energy_list.size) 
        # plt.title("Convergence Curve") 
        # plt.xlabel("iter num") 
        # plt.ylabel("energy") 
        # plt.ylim(-10.0,0)
        # plt.plot(x,energy_list) 
        # plt.show()

    return energy_i[0,0]

if __name__ == "__main__":
    energy_vqe = HE_vqe_test(True)
    energy_fci = -7.882362286798725
    print('err = %20.16f'%(abs(energy_vqe - energy_fci)))
