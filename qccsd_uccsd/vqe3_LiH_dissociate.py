import os
import sys
import numpy as np
from matplotlib import pyplot as plt 
from openfermionpyscf import run_pyscf
import mindquantum as mq
from mindquantum import Circuit, X, RX, RY, RZ, Hamiltonian, StateEvolution
from mindquantum.circuit import generate_uccsd
from mindquantum.nn import generate_pqc_operator
import mindspore as ms
from mindspore import Tensor
from mindspore import dtype as mstype
import mindspore.context as context
from mindspore.common.parameter import Parameter
from mindspore.common.initializer import initializer
from openfermion.chem import MolecularData
from mindquantum.nn import MindQuantumAnsatzOnlyLayer as MAL
from mindquantum.ops import QubitOperator
from mindquantum.ansatz import QubitUCCAnsatz
from mindquantum.hiqfermion.ucc import get_qubit_hamiltonian
from openfermion.ops import InteractionOperator
from openfermion.transforms import get_fermion_operator, jordan_wigner

# np.set_printoptions(threshold=np.inf)
context.set_context(mode=context.GRAPH_MODE, device_target="CPU")
TOLERANCE = 1e-6

def print_state(state_array, n_qubits):
    for i, value in enumerate(state_array):
        if abs(value) > TOLERANCE:
            ang = np.angle(state_array[i])/np.pi
            ket = bin(i)[2:].zfill(n_qubits)
            print("%7.4f %7.4f%s |%s>" % (float(abs(value)), float(ang), chr(960), ket))

def QCCSD(geometry, print_tag=True):
    basis = "sto3g"
    spin = 0

    molecule_of = MolecularData(geometry, basis, multiplicity=2 * spin + 1)
    molecule_of = run_pyscf(molecule_of, run_scf=1, run_ccsd=1, run_fci=1)

    hf_state_circuit = Circuit(
        [X.on(i) for i in range(0,molecule_of.n_electrons,1)])
    # print(hf_state_circuit)

    hamiltonian_QubitOp = get_qubit_hamiltonian(molecule_of)
    n_qubits = molecule_of.n_qubits
    n_electrons = molecule_of.n_electrons
    hamiltonian_InteractionOperator = molecule_of.get_molecular_hamiltonian()
    hamiltonian_FermionOperator=get_fermion_operator(hamiltonian_InteractionOperator)

    eff_hamiltonian_FermionOperator = hamiltonian_FermionOperator
    eff_hamiltonian_QubitOperator = jordan_wigner(eff_hamiltonian_FermionOperator)
    eff_hamiltonian_QubitOperator.compress()

    n_qubits=molecule_of.n_qubits
    depth = 1
    ansatz0 = QubitUCCAnsatz(n_qubits, n_electrons, trotter_step = depth)
    total_circuit = hf_state_circuit + ansatz0.circuit
    # print(total_circuit)

    molecule_pqc = generate_pqc_operator(["null"], total_circuit.para_name, RX("null").on(0) + total_circuit, Hamiltonian(eff_hamiltonian_QubitOperator))
    net = MAL(total_circuit.para_name, total_circuit, Hamiltonian(eff_hamiltonian_QubitOperator))
    opti = ms.nn.Adagrad(net.trainable_params(), learning_rate=4e-1)
    train_net = ms.nn.TrainOneStepCell(net, opti)
    if print_tag == True:
        print(total_circuit.summary())
        print(total_circuit.parameter_resolver())
        print(net.trainable_params())

    eps = 1.e-8
    iter_idx = 0
    min_iter = 5
    energy_diff = eps * 1000
    energy_last = np.zeros(1).astype(np.float32)
    energy_list = np.zeros([0]).astype(np.float32)
    energy_diff_list = np.zeros([0]).astype(np.float32)
    encoder_data = Tensor(np.zeros([1,1]).astype(np.float32))

    

    while (abs(energy_diff) > eps) or (iter_idx < min_iter):
        train_net()
        measure_result, encoder_grad, ansatz_grad = molecule_pqc(encoder_data, net.weight)
        energy_i = measure_result.asnumpy()
        ansatz_grad_norm = np.sqrt(np.sum(ansatz_grad.asnumpy()[0,0]**2))
        energy_diff = energy_i - energy_last
        energy_last = energy_i
        energy_list = np.append(energy_list, energy_i)
        energy_diff_list = np.append(energy_diff_list, energy_diff)
        if ((iter_idx % 1 == 0) and (print_tag == True)):
            print("Step %3d energy    %20.16f    %20.16f    %20.16f" % (iter_idx, float(energy_i), float(energy_diff), float(ansatz_grad_norm)))
            # print(net.weight.asnumpy())
            # print(ansatz_grad.asnumpy()[0,0])
        
        iter_idx += 1
        
    
    

    if print_tag == True:
        print("Optimization completed at step %3d" % (iter_idx - 1))
        print("Optimized energy: %20.16f" % (energy_i))

    return molecule_of.hf_energy, molecule_of.ccsd_energy, molecule_of.fci_energy, float(energy_i[0,0])

def UCCSD_vqe(geometry, print_tag=True):
    basis = "sto3g"
    spin = 0

    molecule_of = MolecularData(geometry, basis, multiplicity=2 * spin + 1)
    molecule_of = run_pyscf(molecule_of, run_scf=1, run_ccsd=1, run_fci=1)
    molecule_of.save()
    molecule_file = molecule_of.filename

    hf_state_circuit = Circuit(
        [X.on(i) for i in range(0,molecule_of.n_electrons,1)])
    # print(hf_state_circuit)

    ansatz_circuit, init_amplitudes, ansatz_parameter_names, hamiltonian_QubitOp, n_qubits, n_electrons = generate_uccsd(molecule_file, th=-1)
    total_circuit = hf_state_circuit + ansatz_circuit
    init_amplitudes = Tensor(np.array(init_amplitudes).astype(np.float32))

    molecule_pqc = generate_pqc_operator(["null"], total_circuit.para_name, RX("null").on(0) + total_circuit, Hamiltonian(hamiltonian_QubitOp))
    net = MAL(total_circuit.para_name, total_circuit, Hamiltonian(hamiltonian_QubitOp), init_amplitudes)
    opti = ms.nn.Adagrad(net.trainable_params(), learning_rate=4e-1)
    train_net = ms.nn.TrainOneStepCell(net, opti)
    if print_tag == True:
        print(total_circuit.summary())
        print(total_circuit.parameter_resolver())
        print(net.trainable_params())

    eps = 1.e-8
    iter_idx = 0
    min_iter = 5
    energy_diff = eps * 1000
    energy_last = np.zeros(1).astype(np.float32)
    energy_list = np.zeros([0]).astype(np.float32)
    energy_diff_list = np.zeros([0]).astype(np.float32)
    encoder_data = Tensor(np.zeros([1,1]).astype(np.float32))

    

    while (abs(energy_diff) > eps) or (iter_idx < min_iter):
        train_net()
        measure_result, encoder_grad, ansatz_grad = molecule_pqc(encoder_data, net.weight)
        energy_i = measure_result.asnumpy()
        ansatz_grad_norm = np.sqrt(np.sum(ansatz_grad.asnumpy()[0,0]**2))
        energy_diff = energy_i - energy_last
        energy_last = energy_i
        energy_list = np.append(energy_list, energy_i)
        energy_diff_list = np.append(energy_diff_list, energy_diff)
        if ((iter_idx % 1 == 0) and (print_tag == True)):
            print("Step %3d energy    %20.16f    %20.16f    %20.16f" % (iter_idx, float(energy_i), float(energy_diff), float(ansatz_grad_norm)))
            # print(net.weight.asnumpy())
            # print(ansatz_grad.asnumpy()[0,0])
        
        iter_idx += 1
        
    
    

    if print_tag == True:
        print("Optimization completed at step %3d" % (iter_idx - 1))
        print("Optimized energy: %20.16f" % (energy_i))

    return float(energy_i[0,0])

if __name__ == "__main__":
    with open(sys.path[0]+'/'+'vqe3_LiH_dissociate.txt', 'a+', buffering=1) as f:
        for dist in np.arange(1.2,5,0.1):
            geometry = [
                ["Li", [0.0, 0.0, 0.0 * dist]],
                ["H", [0.0, 0.0, 1.0 * dist]],
            ]
            # energy_hf, energy_ccsd, energy_fci, energy_vqe = QCCSD(geometry, True)
            # f.write("%4.2f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
            # print("%4.2f\t%20.16f\t%20.16f\t%20.16f\t%20.16f\n"%(dist, energy_hf, energy_ccsd, energy_fci, energy_vqe))
            energy_vqe = UCCSD_vqe(geometry, True)
            f.write("%4.2f\t%20.16f\n"%(dist, energy_vqe))
            print("%4.2f\t%20.16f\n"%(dist, energy_vqe))
    
    # energy_hf, energy_ccsd, energy_fci, energy_vqe = Exgate_vqe3(geometry, True)
    # # print('err = %20.16f'%(abs(energy_vqe - energy_fci)))
    # print(energy_hf)
    # print(energy_ccsd)
    # print(energy_fci)
    # print(energy_vqe)
