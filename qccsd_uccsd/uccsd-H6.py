import os
import sys
import numpy as np
from matplotlib import pyplot as plt 
from openfermionpyscf import run_pyscf
import mindquantum as mq
from mindquantum import Circuit, X, RX, RY, RZ, Hamiltonian, StateEvolution
from mindquantum.circuit import generate_uccsd
from mindquantum.nn import generate_pqc_operator
import mindspore as ms
from mindspore import Tensor
from mindspore import dtype as mstype
import mindspore.context as context
from mindspore.common.parameter import Parameter
from mindspore.common.initializer import initializer
from openfermion.chem import MolecularData
from mindquantum.nn import MindQuantumAnsatzOnlyLayer as MAL
from mindquantum.ops import QubitOperator
from mindquantum.ansatz import QubitUCCAnsatz
from mindquantum.hiqfermion.ucc import get_qubit_hamiltonian
from openfermion.ops import InteractionOperator
from openfermion.transforms import get_fermion_operator, jordan_wigner
from scipy.optimize import minimize

# np.set_printoptions(threshold=np.inf)
os.environ['OMP_NUM_THREADS'] = '4'
ms.context.set_context(mode=ms.context.GRAPH_MODE, device_target="CPU")
TOLERANCE = 1e-6

def print_state(state_array, n_qubits):
    for i, value in enumerate(state_array):
        if abs(value) > TOLERANCE:
            ang = np.angle(state_array[i])/np.pi
            ket = bin(i)[2:].zfill(n_qubits)
            print("%7.4f %7.4f%s |%s>" % (float(abs(value)), float(ang), chr(960), ket))

def UCCSD_vqe(geometry, print_tag=True):
    basis = "sto3g"
    spin = 0

    molecule_of = MolecularData(geometry, basis, multiplicity=2 * spin + 1)
    molecule_of = run_pyscf(molecule_of, run_scf=1, run_ccsd=1, run_fci=1)
    molecule_of.save()
    molecule_file = molecule_of.filename

    hf_state_circuit = Circuit(
        [X.on(i) for i in range(0,molecule_of.n_electrons,1)])
    # print(hf_state_circuit)

    ansatz_circuit, _, _, hamiltonian_QubitOp, _, _ = generate_uccsd(molecule_file, th=-1)
    total_circuit = hf_state_circuit + ansatz_circuit
    init_amplitudes = [0.0 for j in range(len(total_circuit.para_name))]
    molecule_pqc = generate_pqc_operator(["null"], total_circuit.para_name, RX("null").on(0) + total_circuit, Hamiltonian(hamiltonian_QubitOp))
    # a, b = energy_obj(init_amplitudes, molecule_pqc)
    if print_tag == True:
        print(total_circuit.summary())
        print(total_circuit.parameter_resolver())

    iter_no = 0
    energy_last = 0.0
    def call_fun_IterStep(paras, molecule_pqc):
        nonlocal iter_no
        nonlocal energy_last
        energy, grad = energy_obj(paras, molecule_pqc)
        energy_diff = energy - energy_last
        energy_last = energy
        ansatz_grad_norm = np.sqrt(np.sum(grad**2))
        print("Step %3d energy    %20.16f    %20.16f    %20.16f" % (iter_no, float(energy), float(energy_diff), float(ansatz_grad_norm)))
        iter_no += 1

    def energy_obj(n_paras, mol_pqc):
        encoder_data = Tensor(np.array([[0]]).astype(np.float32))
        ansatz_data = Tensor(np.array(n_paras).astype(np.float32))
        e, _, grad = mol_pqc(encoder_data, ansatz_data)
        return e.asnumpy()[0, 0], grad.asnumpy()[0, 0]

    res = minimize(energy_obj,
        init_amplitudes,
        args=(molecule_pqc, ),
        method='bfgs',
        options={'disp': print_tag, 'return_all': True},
        jac=True,
        callback=call_fun_IterStep, 
        tol=1e-6)
        
    print("VQE energy with UCCSD0 ansatz:{}".format(float(res.fun)))
    print("Corresponding parameters:{}".format(res.x.tolist()))

    return float(res.fun)

dist = 1.0
geometry = [
    ["H", [0.0, 0.0, 0.0]],
    ["H", [dist, 0.0, 0.0]],
    ["H", [dist * 2.0, 0.0, 0.0]],
    ["H", [dist * 3.0, 0.0, 0.0]],
    ["H", [dist * 4.0, 0.0, 0.0]],
    ["H", [dist * 5.0, 0.0, 0.0]],
]
energy_vqe = UCCSD_vqe(geometry, True)
print("%4.2f\t%20.16f\n"%(dist, energy_vqe))

# if __name__ == "__main__":
#     with open(sys.path[0]+'/'+'UCCSD-vqe_H6_dissociate.txt', 'a+', buffering=1) as f:
#         for dist in np.arange(0.5, 2.5, 0.05):
#             geometry = [
#                 ["H", [0.0, 0.0, 0.0]],
#                 ["H", [dist, 0.0, 0.0]],
#                 ["H", [dist * 2.0, 0.0, 0.0]],
#                 ["H", [dist * 3.0, 0.0, 0.0]],
#                 ["H", [dist * 4.0, 0.0, 0.0]],
#                 ["H", [dist * 5.0, 0.0, 0.0]],
#             ]
#             energy_vqe = UCCSD_vqe(geometry, True)
#             f.write("%4.2f\t%20.16f\n"%(dist, energy_vqe))
#             print("%4.2f\t%20.16f\n"%(dist, energy_vqe))
    
